package com.blackid.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PreferenceConnector{
	public static final String PREF_NAME = "USER_PREFERENCES";
	public static final int MODE = Context.MODE_PRIVATE;
	
	public static final String USER_NAME = "USER_NAME";
	public static final String PASSWORD = "PASSWORD";
	public static final String CHECKED = "CHECKED";
	public static final String LOGGED = "LOGGED";
	
	public static final String USERID = "USER_ID";
	public static final String FNAME = "FNAME";
	public static final String LNAME = "LNAME";
	public static final String EMAIL = "EMAIL";
	public static final String ADDRESS = "ADDRESS";
	public static final String PHONE = "PHONE";
	public static final String TOKEN = "TOKEN";
	public static final String CITY = "CITY";
	public static final String STATE = "STATE";
	//public static final String COUNTRY = "COUNTRY";
	public static final String COMPANY_NAME = "COMPANY_NAME";
	public static final String ABN_NO = "ABN_NO";
	public static final String WEBSITE = "WEBSITE";
	public static final String BANK_TARANSFER_AC = "BANK_TARANSFER_AC";
	public static final String GST_REGISTERED_FIELD = "GST_REGISTERED_FIELD";
	public static final String GST_REGISTERED_NUMBER = "GST_REGISTERED_NUMBER";
	public static final String GST_REGISTERED_PERCENTAGE = "GST_REGISTERED_PERCENTAGE";
	public static final String PROFILE_PIC_FILE = "PROFILE_PIC_FILE";
	public static final String CUSTOMER_CARE_EMAIL= "CUSTOMER_CARE_EMAIL";
	

	public static void writeBoolean(Context context, String key, boolean value) {
		getEditor(context).putBoolean(key, value).commit();
	}

	public static boolean readBoolean(Context context, String key, boolean defValue) {
		return getPreferences(context).getBoolean(key, defValue);
	}

	public static void writeInteger(Context context, String key, int value) {
		getEditor(context).putInt(key, value).commit();

	}

	public static int readInteger(Context context, String key, int defValue) {
		return getPreferences(context).getInt(key, defValue);
	}

	public static void writeString(Context context, String key, String value) {
		getEditor(context).putString(key, value).commit();

	}
	
	public static String readString(Context context, String key, String defValue) {
		return getPreferences(context).getString(key, defValue);
	}
	
	public static void writeFloat(Context context, String key, float value) {
		getEditor(context).putFloat(key, value).commit();
	}

	public static float readFloat(Context context, String key, float defValue) {
		return getPreferences(context).getFloat(key, defValue);
	}
	
	public static void writeLong(Context context, String key, long value) {
		getEditor(context).putLong(key, value).commit();
	}

	public static long readLong(Context context, String key, long defValue) {
		return getPreferences(context).getLong(key, defValue);
	}

	public static SharedPreferences getPreferences(Context context) {
		return context.getSharedPreferences(PREF_NAME, MODE);
	}

	public static Editor getEditor(Context context) {
		return getPreferences(context).edit();
	}

}
