package com.blackid.imgeload;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;
import android.widget.Toast;

public class Imagelaod_new {

	static Context context;
	static FileCache fileCache;
	MemoryCache memoryCache=new MemoryCache();
	public Imagelaod_new(Context context){
		 this.context=context;
		 fileCache=new FileCache(context);
	}
	 
	 
	 
	public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) 
	{
		try {
			int width = bm.getWidth();
			int height = bm.getHeight();
			float scaleWidth = ((float) newWidth) / width;
			float scaleHeight = ((float) newHeight) / height;
			// create a matrix for the manipulation
			Matrix matrix = new Matrix();
			// resize the bit map
			matrix.postScale(scaleWidth, scaleHeight);
			// recreate the new Bitmap
			Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
			return resizedBitmap;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static Bitmap loadBitmap(String URL) {
		
		BitmapFactory.Options bmOptions;
	    bmOptions = new BitmapFactory.Options();
	    bmOptions.inSampleSize = 1;
	    
		File f=fileCache.getFile(URL);
		//from SD cache
		if(f.exists())
		{
			Bitmap b;
			try {
				b = BitmapFactory.decodeStream(new FileInputStream(f),null,bmOptions);
				if(b!=null)
				{
					Log.i("tag","HEllo1");
					return b;
					
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
				
		//from web	
		Bitmap bitmap = null;
        InputStream in = null;
        try {
        	in = OpenHttpConnection(URL);
            if(in!=null)
            {
            	bitmap = BitmapFactory.decodeStream(in, null, bmOptions);
            	OutputStream os = new FileOutputStream(f);
                Utils.CopyStream(in, os);
                os.close();
	            in.close();
            }
        	
        } catch (IOException e1) {
        }
       
        return bitmap;
    }
	
	
	private static InputStream OpenHttpConnection(String strURL)
            throws IOException {
        InputStream inputStream = null;
        URL url = new URL(strURL);
        URLConnection conn = url.openConnection();

        try {
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setRequestMethod("POST");
            httpConn.connect();

            if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                inputStream = httpConn.getInputStream();
            }
        } catch (Exception ex) {
        }
        return inputStream;
    }
	
	public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }
}
