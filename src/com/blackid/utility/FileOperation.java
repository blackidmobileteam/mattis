package com.blackid.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class FileOperation {

	String content;
	Context context;
	
	public FileOperation(Context con)
	{
		context=con;
	}
	
	
	public FileOperation(Context con,String str_content)
	{
		context=con;
		content=str_content;
	}
	
	public Boolean checkExternalStorage()
	{
		
		String state = Environment.getExternalStorageState();
		
		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    // We can read and write the media
		    return true;
		}
		return false;
	}
	
	
	
	public boolean prepareDirectory( String tempdir){
	    try {
	        if (makedirs(tempdir)){
	            return true;
	        } else {
	            return false;
	        }
	    } catch (Exception e){
	        e.printStackTrace();
	        Toast.makeText(context, "Could not initiate File System.. Is Sdcard mounted properly?", Toast.LENGTH_LONG).show();
	        return false;
	    }
	}

	public boolean makedirs(String tempDir){
	    File tempdir = new File(tempDir);
	    if (!tempdir.exists())
	        tempdir.mkdirs();
	    
	    return (tempdir.isDirectory());
	}
	
	
	public void writeToSDFile(String filename){
        
       File file = new File(filename);
    
        try {
            FileOutputStream fout = new FileOutputStream(file,false);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fout);
            outputStreamWriter.write(content);
            outputStreamWriter.close();
            fout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.i("TAG", "******* File not found. Did you" +
                            " add a WRITE_EXTERNAL_STORAGE permission to the manifest?");
        } catch (IOException e) {
            e.printStackTrace();
        }	
        
    }
	
	public String readFromSDfile(String fileName) {
		// TODO Auto-generated method stub
		
		
		File file = new File(fileName);
		String ret = "";
		        
		        try {
		            FileInputStream fin=new FileInputStream(file);
		        	//InputStream inputStream = openFileInput("my.txt");
		            InputStreamReader inputStreamReader = new InputStreamReader(fin);
		            if ( inputStreamReader != null ) {
		            	
		            	BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		            	String receiveString = "";
		            	StringBuilder stringBuilder = new StringBuilder();
		            	
		            	while ( (receiveString = bufferedReader.readLine()) != null ) {
		            		stringBuilder.append(receiveString);
		            	}
		            	
		            	inputStreamReader.close();
		            	ret = stringBuilder.toString();
		            }
		        }
		        catch (FileNotFoundException e) {
		        	Log.e("TAG", "File not found: " + e.toString());
				} catch (IOException e) {
					Log.e("TAG", "Can not read file: " + e.toString());
				}
		Log.i("FILE NAME FROM SDFILE",fileName);
		//Log.i("FILE CONTENT FROM SDFILE",ret);
		        return ret;
			
	}
	
	
	public Boolean isFileExist(String filename)
	{
		File file = new File(filename);
		
		if(file.exists())
		{
			return true;
		}
		else
		{
			return false;
		
		}
		
	}
	
}
