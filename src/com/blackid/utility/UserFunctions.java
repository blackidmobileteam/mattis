
package com.blackid.utility;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.blackid.constant.Constant;
import com.blackid.jsonparser.Jsonparser;
import android.util.Log;

public class UserFunctions {
	
	private Jsonparser jsonParser;
	
	private static String login_tag = "login";
	private static String register_tag = "signup";
	private static String update_tag = "update_profile";
	
	// constructor
	public UserFunctions(){
		jsonParser = new Jsonparser();
	}
	
	
	public JSONObject loginUser(String email, String password){
		// Building Parameters
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("action", login_tag));
		params.add(new BasicNameValuePair("user_name", email));
		params.add(new BasicNameValuePair("user_password", password));
		
		String json_str=jsonParser.getJSONFromUrl(Constant.URL, params);
		JSONObject json = null;
		try {
			json = new JSONObject(json_str);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// return json
		if(jsonParser.code!=200)
		{
			json=null;
		}
		return json;
	}
	
	
	public JSONObject registerUser(String uname,String password,String cpassword,String email)
	{

		// Building Parameters
		Log.e("JSON", "Into Register1");
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("action", register_tag));
		params.add(new BasicNameValuePair("user_name", uname));
		params.add(new BasicNameValuePair("user_password", password));
		params.add(new BasicNameValuePair("user_cpassword", cpassword));
		params.add(new BasicNameValuePair("user_email", email));
		
		Log.e("JSON", "Into Register2");
		// getting JSON Object
		String json_str= jsonParser.getJSONFromUrl(Constant.URL, params);
		JSONObject json = null;
		try {
			json = new JSONObject(json_str);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// return json
		Log.e("JSON", ""+json);
		Log.e("CODE", ""+jsonParser.code);
		if(jsonParser.code!=200)
		{
			json=null;
		}
		return json;
	}
	
	
	public JSONObject updateUser(String uid,String utoken,String password,String cpassword,String fname,String lname,
			String address,String email,String city,String state,String country,String phone, String company_name,String company_abn_no,String cmp_account,
			String company_website,String gst_reg,String gst_registered_no,String profile_pic)
	{
	
		// Building Parameters
		Log.e("JSON", "Into Register1");
		
		List<NameValuePair> list_update = new ArrayList<NameValuePair>();
		list_update.add(new BasicNameValuePair("action", update_tag));
		list_update.add(new BasicNameValuePair("user_id", uid));
		list_update.add(new BasicNameValuePair("user_token", utoken));
		list_update.add(new BasicNameValuePair("password", password));
		list_update.add(new BasicNameValuePair("cpassword", cpassword));
		list_update.add(new BasicNameValuePair("first_name", fname));
		list_update.add(new BasicNameValuePair("last_name", lname));
		list_update.add(new BasicNameValuePair("email", email));
		list_update.add(new BasicNameValuePair("address", address));//Adress
		list_update.add(new BasicNameValuePair("city", city));
		list_update.add(new BasicNameValuePair("state", state));
		//list_update.add(new BasicNameValuePair("country", country));
		list_update.add(new BasicNameValuePair("contact_no", phone));//Contact No
		
		list_update.add(new BasicNameValuePair("companyname", company_name));
		list_update.add(new BasicNameValuePair("business_abn_no", company_abn_no));
		list_update.add(new BasicNameValuePair("business_bank_transfer_account", cmp_account));
		list_update.add(new BasicNameValuePair("business_website", company_website));
		list_update.add(new BasicNameValuePair("gst_registered_field", gst_reg));
		list_update.add(new BasicNameValuePair("gst_registered_number", gst_registered_no));
		list_update.add(new BasicNameValuePair("profile_pic", profile_pic));
		
		
		Log.e("JSON", "Into Register2");
		// getting JSON Object
		String json_str= jsonParser.getJSONFromUrl(Constant.URL, list_update);
		JSONObject json = null;
		try {
			json = new JSONObject(json_str);
		} catch (JSONException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// return json
		Log.e("JSON", ""+json);
		Log.e("CODE", ""+jsonParser.code);
		if(jsonParser.code!=200)
		{
			json=null;
		}
		return json;
	}
	
	
}
