package com.blackid.utility;

import com.blackid.myresidential.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionDetector {
	
	private Context _context;
	
	public ConnectionDetector(Context context){
		this._context = context;
	}

	public boolean isConnectingToInternet(){
		ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
		  if (connectivity != null) 
		  {
			  NetworkInfo[] info = connectivity.getAllNetworkInfo();
			  if (info != null) 
				  for (int i = 0; i < info.length; i++) 
					  if (info[i].getState() == NetworkInfo.State.CONNECTED)
					  {
						  return true;
					  }

		  }
		  return false;
	}
	
	
	public void showSettingAlertDialog()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(_context);
		
		builder.setTitle("Network Connection Problem").setMessage("Unfortunately, The Network Connection is not available").setIcon(R.drawable.yellow_warning_icon);
		// Add the buttons
		builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		              
		        	  _context.startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
		        	  
		        	   //Toast.makeText(JsonDemoMainActivity.this, "COME BACK", Toast.LENGTH_LONG).show();
		        	   
		           }
		       });
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		               // User cancelled the dialog
		        	   ((Activity)_context).finish();
		           }
		       });
		
		builder.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface arg0) {
				// TODO Auto-generated method stub
				
				((Activity)_context).finish();
				
			}
		});
		
		// Set other dialog properties

		// Create the AlertDialog
		AlertDialog dialog = builder.create();
		dialog.show();
		
	}
}
