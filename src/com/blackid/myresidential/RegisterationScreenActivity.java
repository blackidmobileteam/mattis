package com.blackid.myresidential;



import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.blackid.utility.UserFunctions;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterationScreenActivity extends ResidentialApp{

	EditText edit_username,edit_password,edit_cpassword,edit_emailid;
	String str_username,str_password,str_cpassword,str_emailid;
	TextView txt_user_error,txt_pwd_error,txt_email_error,txt_cpwd_error;
	ProgressDialog progress_dialog;
	TextView text_login;
	Button btn_signup;
	View layoutView;
	LinearLayout middleContent;
	ImageView accountImg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		// Addng Title Bar
		/*requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.registration_srceen);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.login_register_header);*/
		
		middleContent 	= (LinearLayout)findViewById(R.id.middle_content);
		accountImg		= (ImageView)findViewById(R.id.account);
		text_login		= (TextView)findViewById(R.id.textView_title);
		
		accountImg.setVisibility(View.GONE);
		text_login.setVisibility(View.VISIBLE);
		
		LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutView	= inflater.inflate(R.layout.registration_srceen, null);
		
		progress_dialog=new ProgressDialog(this);
		progress_dialog.setMessage("Loading...");
		progress_dialog.setCancelable(true);
		progress_dialog.setCanceledOnTouchOutside(false);
		
		//text_login=(TextView)findViewById(R.id.textView_title);
		text_login.setText("Login");
		/*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) 
			
			ActionBar actionbar_fragment=getActionBar();
			actionbar_fragment.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_launcher));
			//actionbar_fragment.setDisplayHomeAsUpEnabled(false);
			//actionbar_fragment.show();
		}*/
	
		
		
		text_login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent_login=new Intent(RegisterationScreenActivity.this, LoginScreenActivity.class);
				startActivity(intent_login);
				RegisterationScreenActivity.this.finish();
			}
		});
		
		
		getAllReference();
		
		
		edit_emailid.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				Boolean valid_email=isEmailValid(edit_emailid.getText().toString());
				if(valid_email==false)
				{
					//edit_emailid.setError("Invalid Email", erroricon);
					txt_email_error.setVisibility(View.VISIBLE);
				}
				else
				{
					txt_email_error.setVisibility(View.GONE);
				}
			}
		});
		
		/*edit_emailid.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				Boolean valid_email=isEmailValid(edit_emailid.getText().toString());
				if(valid_email==false)
				{
					//edit_emailid.setError("Invalid Email", erroricon);
					txt_email_error.setVisibility(View.VISIBLE);
				}
			}
		});*/
		
		
		btn_signup.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getValueOfAll();
				
				if(isEmpty(str_username) && isEmpty(str_password) && 
						isEmpty(str_cpassword) && isEmpty(str_emailid) )
				{
					if(!(str_password.equals(str_cpassword)))
					{
						/*edit_cpassword.setText("");
						edit_cpassword.setHint("Password Missmatch");
						edit_cpassword.setHintTextColor(Color.RED);
						edit_cpassword.requestFocus();*/
						//edit_cpassword.setError("Password Missmatch",erroricon );
						txt_cpwd_error.setVisibility(View.VISIBLE);
					}
					else
					{
						//Toast.makeText(RegisterationScreenActivity.this, "ALL PERFET", Toast.LENGTH_LONG).show();
						new SignUpOpetarion(RegisterationScreenActivity.this).execute();
					}
				}
				/*else
				{
					Toast.makeText(RegisterationScreenActivity.this, "Some Field Missing", Toast.LENGTH_LONG).show();
				}*/
				
			}
		});
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		middleContent.addView(layoutView);
	}
	
	
	public boolean isEmailValid(String email)
    {
		String not_valid="";
		String[] test = email.split(",");
		//Toast.makeText(contextc, test.length+"", Toast.LENGTH_LONG).show();
		String regExpn =
       		 "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
		 
		for(int i=0;i<test.length;i++)
		{
			CharSequence inputStr = test[i];
			Matcher matcher = pattern.matcher(inputStr);
		    if(matcher.matches())
		    {
		    	//return true;
		    }
		    else
		    {
		    	if(i==0 || i==test.length-1)
		    	{
		    		not_valid=not_valid+(i+1);
		    	}
		    	else
		    	{
		    		not_valid=not_valid+(i+1)+",";
		    	}
		    }
		}
		if(not_valid.equalsIgnoreCase(""))
		{
			return true;
		}
		else
		{
			return false;
		}
		
    }
	
	public Boolean isEmpty(String s)
	{
		if(s.equalsIgnoreCase(""))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	private void getAllReference()
	{
		edit_username=(EditText)layoutView.findViewById(R.id.editText_username);
		edit_password=(EditText)layoutView.findViewById(R.id.editText_password);
		edit_cpassword=(EditText)layoutView.findViewById(R.id.editText_Cpassword);
		edit_emailid=(EditText)layoutView.findViewById(R.id.editText_emailid);
		
		txt_user_error=(TextView)layoutView.findViewById(R.id.textView_error_username);
		txt_email_error=(TextView)layoutView.findViewById(R.id.textView_error_email);
		txt_pwd_error=(TextView)layoutView.findViewById(R.id.textView_error_password);
		txt_cpwd_error=(TextView)layoutView.findViewById(R.id.textView_error_cpassword);
		
		btn_signup=(Button)layoutView.findViewById(R.id.button_register);
	}
	
	
	private void getValueOfAll()
	{
		str_username=validateEditText(edit_username,txt_user_error);		
		str_password=validateEditText(edit_password,txt_pwd_error);
		str_cpassword=validateEditText(edit_cpassword,txt_cpwd_error);
		str_emailid=validateEditText(edit_emailid,txt_email_error);
	}
	
	
	private String validateEditText(EditText e,TextView t)
	{
		if(e.getText().toString().equalsIgnoreCase(""))
		{
			/*e.setHint(value);
			e.setHintTextColor(Color.RED);
			e.requestFocus();*/
			t.setVisibility(View.VISIBLE);
			return "";
		}
		else
		{	t.setVisibility(View.GONE);
			return e.getText().toString();
		}
	}
	
	
	class SignUpOpetarion extends AsyncTask< Void, Void,Void>
	{

		Context context;
		UserFunctions userFunction;
		JSONObject json_object;
		String str_status="";
		
		public SignUpOpetarion(Context con)
		{
			context=con;
		}
		

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress_dialog.show();
			userFunction = new UserFunctions();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			json_object=userFunction.registerUser(str_username, str_password, str_cpassword,str_emailid);
			if(json_object!=null)
			{
				try {
					str_status=json_object.getString("status");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return null;
		}
		
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progress_dialog.dismiss();
			if(str_status.equalsIgnoreCase("error"))
			{
				try {
					
					JSONArray json_detail_array=json_object.getJSONArray("detail");
					for(int i=0;i<json_detail_array.length();i++)
					{
						JSONObject j=json_detail_array.getJSONObject(i);
						String key=j.getString("key");
						String value=j.getString("value");
						
						if(key.equalsIgnoreCase("txt_username_error"))
						{
							txt_user_error.setText(value);
							txt_user_error.setVisibility(View.VISIBLE);
						}
						else if(key.equalsIgnoreCase("txt_email_error"))
						{
							txt_email_error.setText(value);
							txt_email_error.setVisibility(View.VISIBLE);
						}
						else if(key.equalsIgnoreCase("txt_cpassword_error"))
						{
							txt_cpwd_error.setText(value);
							txt_cpwd_error.setVisibility(View.VISIBLE);
						}
						else if(key.equalsIgnoreCase("txt_password_error"))
						{
							txt_pwd_error.setText(value);
							txt_pwd_error.setVisibility(View.VISIBLE);
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
			else
			{
				Toast.makeText(RegisterationScreenActivity.this, "Registered Successfully", Toast.LENGTH_LONG).show();
				Intent i=new Intent(context, LoginScreenActivity.class);
				/*i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);*/
				startActivity(i);
				RegisterationScreenActivity.this.finish();
			}
			
		}
		
	}

	/*@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	
	    switch (item.getItemId()) {
	
	    case android.R.id.home: {
	        this.finish();
	
	        return true;
	    }
	
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}*/
	
}
