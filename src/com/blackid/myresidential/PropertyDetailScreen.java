package com.blackid.myresidential;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.ConnectionClosedException;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import com.blackid.constant.Constant;
import com.blackid.imgeload.Imagelaod_new;
import com.blackid.jsonparser.Jsonparser;
import com.blackid.utility.ConnectionDetector;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Process;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebView.WebViewTransport;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PropertyDetailScreen extends ResidentialApp{

	private static String STATUS="status";
	private static String KEY="key";
	private static String PID="pid";
	private static String LIST="list";
	private static String TITLE="title";
	private static String DISCRIPTION="discription";
	private static String CONTARCT_TYPE="contract_type";
	private static String TYPE="type";
	private static String PRICE="price";
	private static String PROPERTY_TYPE="property_type";
	private static String LOCATION="location";
	private static String BEDROOMS="bedrooms";
	private static String BATHROOMS="bathrooms";
	private static String AREA="area";
	private static String AMENITIES="amenities";
	private static String PROPERT_IMG="property_img";
	
	TextView txt_type,txt_price,txt_size,txt_title_add,txt_detail_add;
	
	LinearLayout middleContent;
	ImageView account;
	ConnectionDetector connection_detector;
	ImageView img_property;	
	TextView text_description;
	Button btn_show_less;
	int show_less_flag=0;
	WebView webview_staticmap;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		middleContent	= (LinearLayout)findViewById(R.id.middle_content);
		account			= (ImageView)findViewById(R.id.account);
		
		LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v	= inflater.inflate(R.layout.property_detail_screen, null);
		
		/*StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
        .permitAll().build();
		StrictMode.setThreadPolicy(policy);*/
    
		connection_detector=new ConnectionDetector(this);
		
		img_property=(ImageView)v.findViewById(R.id.imageView_property);
		txt_type=(TextView)v.findViewById(R.id.textView_type);
		txt_price=(TextView)v.findViewById(R.id.textView_price);
		txt_size=(TextView)v.findViewById(R.id.textView_size);
		txt_title_add=(TextView)v.findViewById(R.id.textView_title_add);
		txt_detail_add=(TextView)v.findViewById(R.id.textView_detail_add);
		btn_show_less=(Button)v.findViewById(R.id.button_showless);
		text_description=(TextView)v.findViewById(R.id.textView_description);
		
		
		webview_staticmap=(WebView)v.findViewById(R.id.webView_saticmap);
		webview_staticmap.setEnabled(false);
		
		// disable scroll on touch
		webview_staticmap.setOnTouchListener(new View.OnTouchListener() {

		    @Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				 return (event.getAction() == MotionEvent.ACTION_MOVE);
			}
		});
		
		webview_staticmap.setHorizontalScrollBarEnabled(false);
		webview_staticmap.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		webview_staticmap.setVerticalScrollBarEnabled(false);
		
		
		if(connection_detector.isConnectingToInternet())
		{
			new AsyncPropetyDetail(this).execute();
		}
		else
		{
			connection_detector.showSettingAlertDialog();
		}
		
		account.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(PropertyDetailScreen.this,SignupLoginActivity.class);
				startActivity(i);
			}
		});
		
		//Toast.makeText(PropertyDetailScreen.this, height+"", Toast.LENGTH_LONG).show();
		btn_show_less.setOnClickListener(new OnClickListener() {
			
			int backup_h;
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int height=text_description.getHeight();
				if(height > 50)
				{
					backup_h=height;
				}
				if(show_less_flag==0)
				{
					show_less_flag=1;
					text_description.setHeight(50);
				}
				else
				{
					show_less_flag=0;
					text_description.setHeight(backup_h);
				}
				
				//Toast.makeText(PropertyDetailScreen.this, height+"", Toast.LENGTH_LONG).show();
			}
		});
		middleContent.addView(v);
	}
	
	class AsyncPropetyDetail extends AsyncTask<Void,Void,Void>
	{
		Context context;
		ProgressDialog progress_dialog_property;
		String json_string;
		Jsonparser json_parser;
		String property_img,title,discription,contact_type,price,property_type,location,bedrooms,bathrooms,area,amenities;
		Bitmap bm_image;
		Imagelaod_new img_loadernew;
		public AsyncPropetyDetail(Context con)
		{
			context=con;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress_dialog_property=new ProgressDialog(context);
			progress_dialog_property.setMessage("Loading..");
			progress_dialog_property.setCancelable(true);
			progress_dialog_property.setCanceledOnTouchOutside(false);
			progress_dialog_property.setOnCancelListener(new OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					PropertyDetailScreen.this.finish();
				}
			});
			progress_dialog_property.show();
			json_parser=new Jsonparser();
			img_loadernew=new Imagelaod_new(PropertyDetailScreen.this);
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			try {	
				json_string=json_parser.getJsonfromUrl(Constant.URL+"action=property_details&pid="+"177");
				JSONObject json_object=new JSONObject(json_string);
				
				String status		=	json_object.getString(STATUS);
				String key			=	json_object.getString(KEY);
				String pid			=	json_object.getString(PID);
				
				String list			=	json_object.getString(LIST);
				JSONObject json_object_list=new JSONObject(list);
				
				title			=	json_object_list.getString(TITLE);
				discription		=	json_object_list.getString(DISCRIPTION);
				contact_type	=	json_object_list.getString(CONTARCT_TYPE);
				price			=	json_object_list.getString(PRICE);
				property_type	=	json_object_list.getString(PROPERTY_TYPE);
				location		=	json_object_list.getString(LOCATION);
				bedrooms		=	json_object_list.getString(BEDROOMS);
				bathrooms		=	json_object_list.getString(BATHROOMS);
				area			=	json_object_list.getString(AREA);
				amenities		=	json_object_list.getString(AMENITIES);
				property_img	=	json_object_list.getString(PROPERT_IMG);
				
				Log.i("status",status);
				Log.i("key",key);
				Log.i("pid",pid);
				Log.i("list",list);
				Log.i("title",title);
				Log.i("discription",discription);
				Log.i("contact_type",contact_type);
				Log.i("price",price);
				Log.i("property_type",property_type);
				Log.i("location",location);
				Log.i("bedrooms",bedrooms);
				Log.i("bathrooms",bathrooms);
				Log.i("area",area);
				Log.i("amenities",amenities);
				Log.i("property_img",property_img);
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
						
			bm_image = img_loadernew.loadBitmap(property_img);
				     
			if(bm_image != null)
			{
				bm_image=img_loadernew.getResizedBitmap(bm_image, img_property.getHeight(), img_property.getWidth());
			}
			
			
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			// TODO Auto-generated method stub
			//webview_staticmap.loadUrl("http://maps.googleapis.com/maps/api/staticmap?center=22.304939,73.208771&zoom=17&size=400x300&scale=2&maptype=roadmap&sensor=false&&markers=color:red|label:H|22.304939,73.208771");
			img_property.setImageBitmap(bm_image);	
			
			super.onPostExecute(result);
			if(progress_dialog_property.isShowing())
			{
				progress_dialog_property.dismiss();
			}
			
		}
	
	}
	
	
	public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) 
	{
		try {
			int width = bm.getWidth();
			int height = bm.getHeight();
			float scaleWidth = ((float) newWidth) / width;
			float scaleHeight = ((float) newHeight) / height;
			// create a matrix for the manipulation
			Matrix matrix = new Matrix();
			// resize the bit map
			matrix.postScale(scaleWidth, scaleHeight);
			// recreate the new Bitmap
			Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
			return resizedBitmap;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	 public static Bitmap loadBitmap(String URL, BitmapFactory.Options options) {
	        Bitmap bitmap = null;
	        InputStream in = null;
	        try {
	        	in = OpenHttpConnection(URL);
	            if(in!=null)
	            {
	            	bitmap = BitmapFactory.decodeStream(in, null, options);
		            in.close();
	            }
	        	
	        } catch (IOException e1) {
	        }
	        return bitmap;
	    }
	 
	 private static InputStream OpenHttpConnection(String strURL)
	            throws IOException {
	        InputStream inputStream = null;
	        URL url = new URL(strURL);
	        URLConnection conn = url.openConnection();

	        try {
	            HttpURLConnection httpConn = (HttpURLConnection) conn;
	            httpConn.setRequestMethod("POST");
	            httpConn.connect();

	            if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
	                inputStream = httpConn.getInputStream();
	            }
	        } catch (Exception ex) {
	        }
	        return inputStream;
	    }

}
