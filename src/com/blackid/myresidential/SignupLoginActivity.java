package com.blackid.myresidential;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

public class SignupLoginActivity extends ResidentialApp{
	
	LinearLayout middleContent;
	Button signupBtn,loginBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		middleContent = (LinearLayout)findViewById(R.id.middle_content);
		
		LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v	= inflater.inflate(R.layout.signup_login_layout, null);
		
		signupBtn	= (Button)v.findViewById(R.id.sign_up);
		loginBtn	= (Button)v.findViewById(R.id.log_in);
		
		signupBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SignupLoginActivity.this,RegisterationScreenActivity.class);
				startActivity(i);
			}
		});
		
		loginBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SignupLoginActivity.this,LoginScreenActivity.class);
				startActivity(i);
			}
		});
		
		middleContent.addView(v);
	}

}
