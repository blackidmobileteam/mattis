package com.blackid.myresidential;


import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class ForgetPasswordScreenActivity extends Activity{

	
	TextView text_login;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		// Addng Title Bar
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.forgot_password_srceen);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.login_register_header);
		
		
		text_login=(TextView)findViewById(R.id.textView_title);
		text_login.setText("Login");
		
		/*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) 
			
			ActionBar actionbar_fragment=getActionBar();
			actionbar_fragment.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_launcher));
			//actionbar_fragment.setDisplayHomeAsUpEnabled(false);
			//actionbar_fragment.show();
		}*/
		
		text_login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent_login=new Intent(ForgetPasswordScreenActivity.this, LoginScreenActivity.class);
				startActivity(intent_login);
				ForgetPasswordScreenActivity.this.finish();
			}
		});
	
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}
	
	

	/*@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	
	    switch (item.getItemId()) {
	
	    case android.R.id.home: {
	        this.finish();
	
	        return true;
	    }
	
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}*/
	
}
