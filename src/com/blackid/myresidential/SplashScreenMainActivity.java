package com.blackid.myresidential;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

public class SplashScreenMainActivity extends Activity {

	
	private int progressBarStatus = 0;


	private Handler progressBarHandler = new Handler();
	ProgressBar progress_bar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen_main);
		// removing status bar in
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// reset progress bar status
		progressBarStatus = 0;
		
		progress_bar = (ProgressBar) findViewById(R.id.progressBar1);
		
		Drawable customDrawable= getResources().getDrawable(R.drawable.custom_progressbar);
		progress_bar.setProgressDrawable(customDrawable); 
		progress_bar.setProgress(0);
		//Log.i("START","START Services");
		//startService(new Intent(SplashScreenMainActivity.this, Services_cache.class));
		
		new Thread(new Runnable() {
			public void run() {
				while (progressBarStatus < 100) {

					// process some tasks
					progressBarStatus++;

					// your computer is too fast, sleep 1 second
					try {
						Thread.sleep(30);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					// Update the progress bar
					progressBarHandler.post(new Runnable() {
						public void run() {
							progress_bar.setProgress(progressBarStatus);
						}
					});
				}
				
				
				
				// ok, file is downloaded,
				if (progressBarStatus >= 20) {

					// sleep 2 seconds, so that you can see the 100%
					/*
					 * try { Thread.sleep(1000); } catch (InterruptedException
					 * e) { e.printStackTrace(); }
					 */

					// close the progress bar dialog
					// progress_bar.dismiss();
					
					
					Intent mainIntent = new Intent(SplashScreenMainActivity.this,PropertyDetailScreen.class);
					//stopService(new Intent(SplashScreenAtivity.this, Services_cache.class));
					//Log.i("STOP","STOP Services");
					
					SplashScreenMainActivity.this.startActivity(mainIntent);
					SplashScreenMainActivity.this.finish();
					
					//Toast.makeText(SplashScreenMainActivity.this, "Splash Screen", Toast.LENGTH_LONG).show();
				}
			}
		}).start();

	}

	
}