package com.blackid.myresidential;


import com.blackid.preferences.PreferenceConnector;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Already_Login extends Activity{

	Button btn_logout;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.alreadylogin);
		btn_logout=(Button)findViewById(R.id.button1);
		btn_logout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				logOut();
			}
		});
	}

	public void logOut()
	{
		SharedPreferences settings = PreferenceConnector.getPreferences(this);
		SharedPreferences.Editor editor = settings.edit();
		editor.remove(PreferenceConnector.LOGGED);
		editor.commit();
		
		final Intent intent = new Intent(this, LoginScreenActivity.class);
		//final Intent intent = new Intent();
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		Already_Login.this.finish();
	}
}
