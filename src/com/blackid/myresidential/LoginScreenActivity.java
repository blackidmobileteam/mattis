package com.blackid.myresidential;


import org.json.JSONException;
import org.json.JSONObject;


import com.blackid.preferences.PreferenceConnector;
import com.blackid.utility.ConnectionDetector;
import com.blackid.utility.UserFunctions;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LoginScreenActivity extends ResidentialApp{

	LinearLayout middleContent;
	ImageView accountImg;
	Button btn_login;
	TextView text_register,test_forget_password,txt_user_error,txt_pwd_error;
	EditText edit_username,edit_password;
	ProgressDialog progress_dialog;
	JSONObject json_object;
	CheckBox check;
	ConnectionDetector internet_check;
	Boolean connected;
	
	
	private static final String TAG_STATUS="status";
	private static final String TAG_USER_ID="user_id";
	private static final String TAG_FNAME="first_name";
	private static final String TAG_LNAME="last_name";
	private static final String TAG_EMAIL="email";
	private static final String TAG_ADDRESS="address";
	private static final String TAG_PHONE="contact_no";
	private static final String TAG_TOKEN="token";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		// Addng Title Bar
		/*requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.login_srceen);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.login_register_header);*/
		
		LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v	= inflater.inflate(R.layout.login_srceen, null);
		
		middleContent	= (LinearLayout)findViewById(R.id.middle_content);
		accountImg		= (ImageView)findViewById(R.id.account);
		text_register	= (TextView)findViewById(R.id.textView_title);
		
		accountImg.setVisibility(View.GONE);
		text_register.setVisibility(View.VISIBLE);
		
		
		txt_user_error=(TextView)v.findViewById(R.id.textView_error_username);
		txt_pwd_error=(TextView)v.findViewById(R.id.textView_error_password);
				
		edit_username=(EditText)v.findViewById(R.id.editText_username);
		edit_password=(EditText)v.findViewById(R.id.editText_password);
				
		/*btn_login=(Button)findViewById(R.id.button_login);
		btn_login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String username=edit_username.getText().toString();
				String password=edit_password.getText().toString();
				if(username.equalsIgnoreCase(""))
				{
					txt_user_error.setVisibility(View.VISIBLE);
				}
				
				if(password.equalsIgnoreCase(""))
				{
					txt_pwd_error.setVisibility(View.VISIBLE);
				}
				
				if((!(username.equalsIgnoreCase(""))) && (!(password.equalsIgnoreCase(""))))
				{
					
				}
				
			}
		});*/
		
		//text_register=(TextView)findViewById(R.id.textView_title);
		text_register.setText("Register");
		
		test_forget_password=(TextView)v.findViewById(R.id.textView_fogotpassword);
		test_forget_password.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent_login=new Intent(LoginScreenActivity.this, ForgetPasswordScreenActivity.class);
				startActivity(intent_login);
				LoginScreenActivity.this.finish();
			}
		});
		
		/*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) 
			
			ActionBar actionbar_fragment=getActionBar();
			actionbar_fragment.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_launcher));
			//actionbar_fragment.setDisplayHomeAsUpEnabled(false);
			//actionbar_fragment.show();
		}*/
		
		text_register.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent_register=new Intent(LoginScreenActivity.this, RegisterationScreenActivity.class);
				startActivity(intent_register);
				LoginScreenActivity.this.finish();
			}
		});
		
		
		
		check=(CheckBox)v.findViewById(R.id.checkBox_remember_me);
	
		String str_logged=PreferenceConnector.readString(LoginScreenActivity.this,PreferenceConnector.LOGGED,"");
		
		if(str_logged.equalsIgnoreCase("logged"))
		{
			Intent intent_login=new Intent(LoginScreenActivity.this, Already_Login.class);
			startActivity(intent_login);
			LoginScreenActivity.this.finish();
		}
		else
		{
			internet_check=new ConnectionDetector(this);
			connected=internet_check.isConnectingToInternet();
			
			if(connected==false)
			{
				internet_check.showSettingAlertDialog();
			}
			else	
			{
						
				readUser();
				
				if(check.isChecked())
				{
					PreferenceConnector.writeString(LoginScreenActivity.this, PreferenceConnector.CHECKED,"yes");
				}
				else
				{
					PreferenceConnector.writeString(LoginScreenActivity.this, PreferenceConnector.CHECKED,"no");
				}
				
				check.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (((CheckBox) v).isChecked())
	                    {
							PreferenceConnector.writeString(LoginScreenActivity.this, PreferenceConnector.CHECKED,"yes");
	                    }
	                    else
	                    {
	                    	PreferenceConnector.writeString(LoginScreenActivity.this, PreferenceConnector.CHECKED,"no");
	                    }
					}
				});
				
				btn_login=(Button)v.findViewById(R.id.button_login);
				btn_login.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						//Log.i("USERNAME",txt_username.getText().toString());
						//Log.i("PASSWORD",txt_password.getText().toString());
						
						progress_dialog=new ProgressDialog(LoginScreenActivity.this);
						progress_dialog.setMessage("Loading...");
						progress_dialog.setCancelable(true);
						progress_dialog.setCanceledOnTouchOutside(false);
											
						if(edit_username.getText().toString().equalsIgnoreCase(""))
						{
							/*txt_error.setText("Username should not be blank");
							txt_error.setVisibility(TextView.VISIBLE);*/
							txt_user_error.setVisibility(TextView.VISIBLE);
							txt_pwd_error.setVisibility(TextView.GONE);
							//img_error.setImageResource(R.drawable.warning_icon);
						}
						else if(edit_password.getText().toString().equalsIgnoreCase(""))
						{
							txt_pwd_error.setVisibility(TextView.VISIBLE);
							txt_user_error.setVisibility(TextView.GONE);
							//img_error.setImageResource(R.drawable.warning_icon);
						}
						else
						{
							txt_pwd_error.setVisibility(TextView.GONE);
							txt_user_error.setVisibility(TextView.GONE);
							new Loginoperation(LoginScreenActivity.this).execute();
						}
						
					}
				});
				
			}
		
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		middleContent.addView(v);
		
	}
	
	
}
	
	class Loginoperation extends AsyncTask<String, String, String>
	{

		Context context;
		
		String str_login_url;
		String str_error;
		String str_status="",str_user_id,str_fname,str_lname,str_email,str_token,str_phone,str_address;
				//,str_city,str_state,str_company_name,str_abn_no,str_website,str_bank_ac,str_gst_registered,str_gst_no,str_gst_per,str_profile_pic,str_customer_care_email;
		
		UserFunctions userFunction;
		public Loginoperation(Context con) {
			// TODO Auto-generated constructor stub
			context=con;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			progress_dialog.show();
			userFunction = new UserFunctions();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			//json_object=json_parser.getLoginJSONFromUrl(str_login_url);
			json_object= userFunction.loginUser(edit_username.getText().toString(), edit_password.getText().toString());
			if(json_object!=null)
			{
				try {
						str_status=json_object.getString(TAG_STATUS);
						if(str_status.equalsIgnoreCase("success"))
						{
							str_user_id=json_object.getInt(TAG_USER_ID)+"";
							str_fname=json_object.getString(TAG_FNAME);
							str_lname=json_object.getString(TAG_LNAME);
							str_email=json_object.getString(TAG_EMAIL);
							str_address=json_object.getString(TAG_ADDRESS);
							str_token=json_object.getString(TAG_TOKEN);
							/*str_city=json_object.getString(TAG_CITY);
							str_state=json_object.getString(TAG_STATE);*/
							//str_country=json_object.getString(TAG_COUNTRY);
							str_phone=json_object.getString(TAG_PHONE);
							
							/*str_company_name=json_object.getString(TAG_COMPANY_NAME);
							str_abn_no=json_object.getString(TAG_ABN_NO);
							str_website=json_object.getString(TAG_WEBSITE);
							str_bank_ac=json_object.getString(TAG_BANK_TARANSFER_AC);
							str_gst_registered=json_object.getString(TAG_GST_REGISTERED_FIELD);
							str_gst_no=json_object.getString(TAG_GST_REGISTERED_NUMBER);
							str_gst_per=json_object.getString(TAG_GST_REGISTERED_PERCETAGE);
							if(str_gst_per.equalsIgnoreCase(""))
							{
								str_gst_per="0";
							}
							str_profile_pic=json_object.getString(TAG_POFILE_PIC);
							str_customer_care_email=json_object.getString(TAG_CUSTOMER_CARE_EMAIL);*/
							
						}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else
			{
				//Toast.makeText(JsonDemoMainActivity.this, "Service Not Available", Toast.LENGTH_LONG).show();
				str_error="Server problem,Try again later";
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			
			
			if(progress_dialog.isShowing())
			{
				progress_dialog.dismiss();
			
			}
				if(json_object==null)
				{
					Toast.makeText(LoginScreenActivity.this, str_error, Toast.LENGTH_LONG).show();
					
				}
				else if(str_status.equalsIgnoreCase("success"))
				{
					
					//txt_error.setText("");
					//img_error.setImageResource(R.color.transparent);
					
					String str_username = edit_username.getText().toString();
					String str_password = edit_password.getText().toString();
					
					String str_status_check=PreferenceConnector.readString(LoginScreenActivity.this,PreferenceConnector.CHECKED, "no");
					if(str_status_check.equalsIgnoreCase("yes"))
					{
						PreferenceConnector.writeString(LoginScreenActivity.this, PreferenceConnector.USER_NAME,str_username);
					
						PreferenceConnector.writeString(LoginScreenActivity.this, PreferenceConnector.PASSWORD,str_password);
					}
					
					PreferenceConnector.writeString(LoginScreenActivity.this, PreferenceConnector.LOGGED,"logged");
					Intent intent_login=new Intent(LoginScreenActivity.this, Already_Login.class);
					
					PreferenceConnector.writeString(LoginScreenActivity.this, PreferenceConnector.USERID,str_user_id);
					PreferenceConnector.writeString(LoginScreenActivity.this, PreferenceConnector.FNAME,str_fname);
					PreferenceConnector.writeString(LoginScreenActivity.this, PreferenceConnector.LNAME,str_lname);
					PreferenceConnector.writeString(LoginScreenActivity.this, PreferenceConnector.EMAIL,str_email);
					PreferenceConnector.writeString(LoginScreenActivity.this, PreferenceConnector.ADDRESS,str_address);
					PreferenceConnector.writeString(LoginScreenActivity.this, PreferenceConnector.TOKEN,str_token);
					/*PreferenceConnector.writeString(FantasticServicesMainActivity.this, PreferenceConnector.CITY,str_city);
					PreferenceConnector.writeString(FantasticServicesMainActivity.this, PreferenceConnector.STATE,str_state);
					//PreferenceConnector.writeString(FantasticServicesMainActivity.this, PreferenceConnector.COUNTRY,str_country);*/
					PreferenceConnector.writeString(LoginScreenActivity.this, PreferenceConnector.PHONE,str_phone);
					
					/*PreferenceConnector.writeString(FantasticServicesMainActivity.this, PreferenceConnector.COMPANY_NAME,str_company_name);
					PreferenceConnector.writeString(FantasticServicesMainActivity.this, PreferenceConnector.ABN_NO,str_abn_no);
					PreferenceConnector.writeString(FantasticServicesMainActivity.this, PreferenceConnector.WEBSITE,str_website);
					PreferenceConnector.writeString(FantasticServicesMainActivity.this, PreferenceConnector.BANK_TARANSFER_AC,str_bank_ac);
					PreferenceConnector.writeString(FantasticServicesMainActivity.this, PreferenceConnector.GST_REGISTERED_FIELD,str_gst_registered);
					PreferenceConnector.writeString(FantasticServicesMainActivity.this, PreferenceConnector.GST_REGISTERED_NUMBER,str_gst_no);
					PreferenceConnector.writeInteger(FantasticServicesMainActivity.this, PreferenceConnector.GST_REGISTERED_PERCENTAGE,Integer.parseInt(str_gst_per));
					PreferenceConnector.writeString(FantasticServicesMainActivity.this, PreferenceConnector.CUSTOMER_CARE_EMAIL,str_customer_care_email);*/
					
					/*if(!(str_profile_pic.equalsIgnoreCase("")))
					{
						PreferenceConnector.writeString(FantasticServicesMainActivity.this, PreferenceConnector.PROFILE_PIC_FILE,"yes");
						file_operation=new FileOperation(FantasticServicesMainActivity.this, str_profile_pic);
						if(file_operation.checkExternalStorage())
						{
							String tempDir = Environment.getExternalStorageDirectory() + "/Fantastic/Photo/" ;
							if(file_operation.prepareDirectory(tempDir))
							{
								file_operation.writeToSDFile(tempDir+"/base64_profile_" + str_user_id+ ".txt");
							}
						}
						else
						{
							Toast.makeText(FantasticServicesMainActivity.this, "sdcard not prepared", Toast.LENGTH_LONG).show();
						}
					}
					else
					{
						PreferenceConnector.writeString(FantasticServicesMainActivity.this, PreferenceConnector.PROFILE_PIC_FILE,"no");
					}*/
					
					
					startActivity(intent_login);
					LoginScreenActivity.this.finish();
					
				}
				else
				{
					edit_password.setText("");
					txt_pwd_error.setText("Invalid Username or Password");
					txt_pwd_error.setVisibility(TextView.VISIBLE);
					//img_error.setImageResource(R.drawable.warning_icon);
				}
				
			}
		}
	
	
	/*
	 * Read the data refer to saved person and visualize them into Edittexts
	 */
	private void readUser() {
		String checked=PreferenceConnector.readString(this,PreferenceConnector.CHECKED, "no");
		if(checked.equalsIgnoreCase("yes"))
		{
			edit_username.setText(PreferenceConnector.readString(this,
				PreferenceConnector.USER_NAME, null));
			edit_password.setText(PreferenceConnector.readString(this,
				PreferenceConnector.PASSWORD, null));
		}
		else
		{
			edit_username.setText("");
			edit_password.setText("");
		}
		
	}

	/*@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	
	    switch (item.getItemId()) {
	
	    case android.R.id.home: {
	        this.finish();
	
	        return true;
	    }
	
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}*/
	
}
